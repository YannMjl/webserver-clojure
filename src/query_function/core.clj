(ns query-function.core)

(require '[clojure.java.io :as io]
         '[clojure.java.jdbc :as j]
         '[clj-time.coerce :as tc])

;-----------------------------------------------------------------------------------------*
;connect to the database                                                                  *
;                                                                                         *
;-----------------------------------------------------------------------------------------*

(def db {:dbtype "postgresql"
         :dbname "cloudrepo_report"
         :host "localhost"
         :user "yann"
         :password "123@abc"})

;-----------------------------------------------------------------------------------------*
;this section content queries functions that read the report data stored in the postgres  *
;database which returns a sub report by organization name or the data uploaded            *
;-----------------------------------------------------------------------------------------*

(defn get-full-report []
  (j/query db ["SELECT DISTINCT ON (organization)\n
                  organization, size, date\n
                  FROM report\nORDER BY organization"])
  )

(defn get-full-report-of-date []
  (j/query db ["SELECT DISTINCT ON (date)\n
                  organization, size, date\n
                  FROM report\nORDER BY date DESC"])
  )

(defn view-by-organization [org-name]

  (j/query db ["SELECT organization, size, date FROM report WHERE organization = ?" org-name]))

(defn view-by-date [input]

   (let [_date (tc/to-sql-date (tc/to-string input))]

        (j/query db ["SELECT organization, size, date FROM report WHERE date = ?" _date])

        )
  )
;-----------------------------------------------------------------------------------------*
