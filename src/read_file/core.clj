(ns read-file.core)

(require '[clojure.data.csv :as csv]
         '[clojure.java.io :as io])


;-----------------------------------------------------------------------------------------*
;this section content functions that do multiple operation on the csv file in order to    *
;get its content and generate a report                                                    *
;-----------------------------------------------------------------------------------------*

;read lines in the csv file
(defn read-lines [file-path]
  (clojure.string/split-lines file-path))

;split the lines on the cvs file
(defn read-split-lines [file-path]
  (->> (clojure.string/split-lines file-path)
       (map #(clojure.string/split % #",")))
  )

;convert string to integer
(defn to-int [s]
  (if (integer? s)
    s
    (try
      (Integer. s)
      (catch Exception e nil))
    )
  )

;read the cvs file
(defn read-column [filename column-index]
  (with-open [reader (io/reader filename)]
    (let [data (clojure.data.csv/read-csv reader)]
      ;; mapv is not lazy, so the csv data will be consumed at this point
      (mapv #(nth % column-index) data))
    )
  )

;return a string vector of space memory used
(defn total-memory-space [filename]
  (->> (read-column filename 2)
       (drop 2)
       (map #(Double/parseDouble %))
       (reduce + 0)
       (hash-map :size)
       )
  )

;returns a string vector organization names
(defn return-all-organization-name [filename]
  (->> (read-column filename 1)
       (drop 2)
       ;;split the string where there is a /
       (map #(clojure.string/split % #"/"))
       (map #(nth % 0))
       ;;remove a backslash \ from string
       (map #(clojure.string/replace % "\"" "")))
  )

;return an organization id, given a line
(defn return-organization-id [input]
  (->> (clojure.string/split-lines input)
       (map #(clojure.string/split % #","))
       (map #(nth % 1))
       ;;split the string where there is a /
       (map #(clojure.string/split % #"/"))
       (map #(nth % 0))
       ;;remove a backslash \ from string
       (map #(clojure.string/replace % "\"" "")))
  )
;----------------------------------------------------------------------------------*







