(use 'clojure.java.io)
(use 'clojure.set)

(ns report-clojure.core
  (:gen-class)
  (:use [compojure.core :refer :all])
  (:require [clojure.data.csv]
            [clojure.java.io]
            [environ.core :refer [env]]
            [read-file.core :as rf]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.util.response :refer [response]]
            [cheshire.core :refer :all]
            [clojure.java.jdbc :as j]
            [clj-time.coerce :as tc]
            [query-function.core :as qf]
            [ring.middleware.defaults :refer :all]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.multipart-params :refer [wrap-multipart-params]]
            [ring.middleware.cors :refer [wrap-cors]]
            [clj-http.client :as chc])
  )

;-----------------------------------------------------------------------------------------*
;this section content functions that do multiple operation on the csv file in order to    *
;get its content and generate a report                                                    *
;-----------------------------------------------------------------------------------------*

;read lines in the csv file
(defn read-lines-in-file [file-path]
  (rf/read-lines (slurp file-path)))

;read and split lines of the csv file
(defn split-lines-in-file [file-path]
  (rf/read-split-lines (slurp file-path)))

;return the total size of memory space used from the csv file
(defn total-memory-used-in-file [file-path]
  (rf/total-memory-space file-path))

;read organization lines and return organization id
(defn organization-names-in-file [file-path]
  (distinct (rf/return-all-organization-name file-path)))

;return an organization id, given a line
(defn return-id [input]
  (apply str (rf/return-organization-id input)) )

;return true or false, given a line and an organization ID
(defn check-for-match [org-name input-line]
  (= org-name (apply str (return-id input-line)) ))

;filter lines by organization id and return the memory space used by the organization
(defn return-memory-space-for-organizationId [org-name input-lines]
  (->> (filter #(= org-name (return-id %)) input-lines)
       (map #(clojure.string/split % #","))
       (map #(nth % 2))
       (map #(re-find #"(\d+)" %))
       (map second)
       (map rf/to-int)
       (reduce +)
       (hash-map :name org-name :size))
  )

;-----------------------------------------------------------------------------------------*
;this section content functions that return a full report from csv file or a vector       *
;of lines                                                                                 *
;-----------------------------------------------------------------------------------------*

;return name and size of memory space used by each organization in the report
(defn return-organizations-name-and-size [input-lines org-list]
  (map (fn [org-name]
         (return-memory-space-for-organizationId org-name input-lines)) org-list)
  )

;return a full report from a vector of lines
(defn get-lines-report [input-lines]
  (let [org-id-list (distinct (map #(apply str (rf/return-organization-id %)) input-lines))]
    (return-organizations-name-and-size input-lines org-id-list))
  )

; read a csv file and return a full report  (organization names and size of memory used)
(defn get-file-report [file-path]
  (let [input-lines (read-lines-in-file file-path)]
    (get-lines-report input-lines)
    )
  )

;-----------------------------------------------------------------------------------------*
;this section content functions that compare two reports and return one join report based *
;on the difference of the two reports                                                     *
;-----------------------------------------------------------------------------------------*

;get organization name from a report
(defn org-report-info [org-name report]
  (first
    (filter #(= (:name %) org-name) report))

  )

;read a line from each report and returns the change in the memory storage used
(defn report-info [org-name report1 report2]
  (let [first-entry (org-report-info org-name report1)
        second-entry (org-report-info org-name report2)
        first-size (:size first-entry)
        second-size (:size second-entry)
        change-in-size ( - second-size first-size)]

    (hash-map :name org-name :change-in-size change-in-size))
  )

;compare two reports
(defn compare-reports [report1 report2]
  (let [org-name (map :name report1) ]
     (map #(report-info % report1 report2) org-name)
    )
  )


;-----------------------------------------------------------------------------------------*
;this section content functions that connect and upload a report into the database        *
;                                                                                         *
;-----------------------------------------------------------------------------------------*

;connect to the database
(def db {:dbtype "postgresql"
         :dbname "cloudrepo_report"
         :host "localhost"
         :user "yann"
         :password "123@abc"})

;create report table into the database (do only
(def report-table (j/create-table-ddl :report [[:id :serial "Primary key"]
                                                  [:organization "VARCHAR (255)"]
                                                  [:size "VARCHAR (20)"]
                                                  [:date "DATE"]]))

(defn create-report-table []
  (j/execute! db report-table))

;get organization name and size from a report line and insert them into the database
(defn get-name-and-size [report date]
  (let [_name (:name report)
        _size (:size report)
        _date (tc/to-sql-date (tc/to-string date))]

    (j/insert! db :report {:organization _name
                            :size _size
                            :date _date})
    )
  )

;upload a report into the report table in cloudrepo report database
(defn upload-report-to-database [report date]
  (map #(get-name-and-size % date) report)
  )

(defn view-report-uploaded [report date]
  (upload-report-to-database report date)
  )


;-----------------------------------------------------------------------------------------*
;this section content queries functions that read the report data stored in the postgres  *
;database which returns a sub report by organization name or the data uploaded            *
;-----------------------------------------------------------------------------------------*

(defn full-report []
  (qf/get-full-report))

(defn full-report-date []
  (qf/get-full-report-of-date))

(defn view-report-by-organizationID [input]
  (qf/view-by-organization input))

(defn view-report-by-date [input]
  (qf/view-by-date input))



(defn upload-file [file]
  (let [file-name (file :filename)
        size (file :size)]
    (do
      (get-file-report file))
    )
  )


;-----------------------------------------------------------------------------------------*
;this section content functions for routes and route handler                              *
;REST API configurations are also set here                                                *
;-----------------------------------------------------------------------------------------*

(defn app-handler [request]
  {:status  200
   :headers {"Content-Type" "application/json"
             "Access-Control-Allow-Origin" "*"
             "Access-Control-Allow-Credentials" "false"
             "Access-Control-Allow-Methods" "POST, GET, OPTIONS"
             "Access-Control-Allow-Headers" "Accept, Content-Type"}
   :body    (generate-string request)})

(defroutes myroutes

           (GET "/" [] (generate-string (full-report)))
           (GET "/date" [] (generate-string (full-report-date)))
           (GET "/date/:input" [input] (generate-string (qf/view-by-date input)))
           (GET "/:input" [input] (generate-string (view-report-by-organizationID input)))

           (POST "/file" {params :params
                          :as req
                          }

                (let [fileparam (get params "file")
                      file (:tempfile fileparam)
                      dateparam (get params "date")
                      date (tc/to-string dateparam)
                      report (get-file-report file)]

                      (println (upload-report-to-database report date))
                  )

                )


           )

;-----------------------------------------------------------------------------------------*
;this section content the main function that start the server                             *
;on local host port 5000                                                                  *
;-----------------------------------------------------------------------------------------*

(defn -main []
  (let [port (Integer. (env :port "5000"))]

    (jetty/run-jetty (wrap-cors (wrap-multipart-params myroutes)
                                :access-control-allow-methods [:get :post :options]
                                ;:access-control-allow-headers [#"Accept, Content-Type"]
                                :access-control-allow-origin [#"http://localhost:4200"]
                                )
                     {:port port :join? false})

    )

  )

;-----------------------------------------------------------------------------------------*

