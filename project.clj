(defproject io.example-yann/report-clojure "0.1.0-SNAPSHOT"
  :description "This project reads a file.cvs and gives a report of cleints on cloudrepo"
  :url "http://example.com/reportcloudrepo"

  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-x256 "0.0.1"]
                 [csv-map "0.1.2"]
                 [org.clojure/data.csv "0.1.4"]
                 [ring "1.6.3"]
                 [ring/ring-jetty-adapter "1.6.3"]
                 [ring/ring-core "1.6.3"]
                 [ring/ring-mock "0.3.2"]
                 [ring/ring-defaults "0.3.1"]
                 [compojure "1.6.0"]
                 [javax.servlet/servlet-api "2.5"]
                 [ring/ring-json "0.5.0-beta1"]
                 [cheshire "5.8.0"]
                 [org.clojure/java.jdbc "0.7.5"]
                 [org.postgresql/postgresql "42.1.4"]
                 [clj-time "0.14.2"]
                 [yesql "0.5.3"]
                 [org.postgresql/postgresql "42.1.4"]
                 [ring-cors "0.1.11"]
                 [clj-http "3.7.0"]
                 [environ "1.1.0"]]

  :min-lein-version "2.0.0"

  ;;lein-ring plugin make the web page reloadable
  :plugins [[lein-ring "0.12.2"]
            [lein-gorilla "0.4.0"]
            [environ/environ.lein "0.3.1"]]

  :hooks [environ.leiningen.hooks]

  ;;instruction for the lein-ring plugin,
  ;; tell the plugin what handler should be monitored
  :ring {:handler report-clojure.core/app-handler
         :auto-reload? true
         :auto-refresh? false}

  :uberjar-name "webserver-standalone.jar"

  :profiles {:dev {:source-paths ["dev"]
                   :dependencies [[org.clojure/java.classpath "0.2.3"]
                                  [org.clojure/tools.namespace "0.3.0-alpha4"]]
                   }
             :uberjar {:main report-clojure.core
                       :aot
                       :all
                       }
             :production {:env {:production true}}

             }


  )
